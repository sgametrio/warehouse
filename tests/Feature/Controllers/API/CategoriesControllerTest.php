<?php

namespace Tests\Feature\Controllers\API;

use Tests\TestCase;
use App\Category;

class CategoriesControllerTest extends TestCase
{
    public function testGetCategories()
    {
        $categories = factory(Category::class, 3)->create();
        $response = $this->json("GET", "/api/categories");
        $response->assertStatus(200);
        // Assert that there are exactly 3 objects
        $response->assertJsonCount(3, "data");
        $response->assertJsonStructure([
            "data" => [
                "*" => [
                    "name"
                ]
            ]
        ]);
    }

    public function testGetCategory()
    {
        $category = factory(Category::class)->create();
        $response = $this->json("GET", "/api/categories/".$category->id);
        $response->assertStatus(200);
        $response->assertJsonStructure([
            "data" => [
                "name"
            ]
        ]);
    }

    public function testGetCategoryNotExistent()
    {
        $category = factory(Category::class)->create();
        $response = $this->withExceptionHandling()->json("GET", "/api/categories/".($category->id + 1));
        $response->assertStatus(404);
    }

    public function testPostCategory() {
        $response = $this->json("POST", "/api/categories", [
            "name" => "Test category",
        ]);
        $response->assertStatus(201);
        // After succesful POST we should return the object created
        $response->assertJsonStructure([
            "data" => [
                "name"
            ]
        ]);
        $this->assertDatabaseHas("categories", ["name" => "Test category"]);
    }

    public function testPostCategoryNotUnique() {
        $category = factory(Category::class)->create([
            "name" => "test",
        ]);
        // This should fail because categories names are unique
        $response = $this->withExceptionHandling()->json("POST", "/api/categories", [
            "name" => "test",
        ]);
        // Status 422 means: Validation error (Unprocessable Entity)
        $response->assertStatus(422);
    }

    public function testUpdateCategory() {
        $category = factory(Category::class)->create([
            "name" => "test",
        ]);
        $response = $this->json("PATCH", "/api/categories/".$category->id, [
            "name" => "test-update",
        ]);
        // After succesful PATCH we should return the updated object
        $response->assertJsonStructure([
            "data" => [
                "name"
            ]
        ]);
        $response->assertStatus(200);
        $this->assertDatabaseHas("categories", ["name" => "test-update", "id" => $category->id]);
    }

    public function testDeleteCategory() {
        $category = factory(Category::class)->create();
        $response = $this->json("DELETE", "/api/categories/".$category->id);
        $response->assertStatus(200);
        $this->assertDatabaseMissing("categories", ["id" => $category->id]);
    }
}

# warehouse

### Database

* Oggetto
  * nome
  * categoria (opzionale)
* Azioni
  * tipo (aggiungi/togli)
  * record_magazzino
  * quantità
* Magazzino
  * oggetti
  * quantità
  * data_acquisto (created_at)
  * data_scadenza (opzionale)
  * descrizione (opzionale)

* Utenti (ruoli e permessi)
  * username
  * password
* Categorie
  * nome
  * icona (?)

### Struttura

Autenticazione degli utenti via sessione

API per CRU(D) un po' su tutto (prima facciamo i test)



